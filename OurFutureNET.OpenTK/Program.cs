﻿#region Подключенные библиотеки

using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using System;
using System.Linq;
//using System.Windows.Forms;
using OpenTK.Windowing.GraphicsLibraryFramework;

#endregion

namespace OurFutureNET.OpenTK
{
    internal class Program
    {
        #region Класс для управления окном

        /// <summary>
        /// Класс для управления окном с использованием библиотеки OpenTK (OpenGL)
        /// </summary>
        public class FutureGame : GameWindow
        {
            #region Объявление переменных

            //Значение изначальных цветов фона
            private float colorFrameRedUpdate = 0.001f;
            private float colorFrameGreenUpdate = 0.002f;
            private float colorFrameBlueUpdate = 0.003f;

            //Скорость изменения каждого цвета
            private float colorFrameRed = 153 / 255f;
            private float colorFrameGreen = 50 / 255f;
            private float colorFrameBlue = 204 / 255f;


            private int fps = 0; //--> Кол-во FPS в секунду

            private float fpsTime = 0.0f; //--> Время обновления одного кадра

            #endregion

            /// <summary>
            /// Конструктор, принимающий начальные настройки
            /// </summary>
            /// <param name="gameWindowSettings">Первоначальных настройки при создании окна</param>
            /// <param name="nativeWindowaSettings"></param>
            public FutureGame(GameWindowSettings gameWindowSettings, NativeWindowSettings nativeWindowaSettings)
                : base(gameWindowSettings, nativeWindowaSettings)
            {
                //Вывод информции в консольное окно нашего приложения
                // Вывод версии OpenTK
                Console.WriteLine(GL.GetString(StringName.Version));
                Console.WriteLine("\n");

                // Вывод информации о разработчике
                Console.WriteLine(GL.GetString(StringName.Vendor));
                Console.WriteLine("\n");

                // Вывод информации о видеокарте 
                Console.WriteLine(GL.GetString(StringName.Renderer));
                Console.WriteLine("\n");

                // Вывод информации о версии языка OpenTK
                Console.WriteLine(GL.GetString(StringName.ShadingLanguageVersion));

                VSync = VSyncMode.On; //--> Обрезание лишних FPS для окна
                CursorVisible = true; //--> Видим ли курср в окне
            }

            #region Основные функции управления окном
            /// <summary>
            /// При инициализации окна
            /// </summary>
            protected override void OnLoad()
            {
                //Задание первоначального цвета фона
                //GL.ClearColor(Color4.Chocolate);
                //GL.ClearColor(red: 153 / 255f,
                //              green: 50 / 255f,
                //              blue: 204 / 255f,
                //              alpha: 0.8f);
                base.OnLoad();
            }

            /// <summary>
            /// При изменении размеров окна
            /// </summary>
            /// <param name="e"></param>
            protected override void OnResize(ResizeEventArgs e)
            {
                base.OnResize(e);
            }

            /// <summary>
            /// Метод при  обновления кадров в окне<br/> 
            /// <i>(Для расчетов)</i>
            /// Связан с <see cref="OnRenderFrame">функией</see> <i><see cref="OnRenderFrame"/></i>
            /// </summary>
            /// <param name="e"></param>
            protected override void OnUpdateFrame(FrameEventArgs e)
            {
                UpdateFPS(e);
                Diskoteka();

                var key = KeyboardState; //--> Текущая нажатая клавиша

                if (key.IsKeyDown(Keys.Escape)) //--> Обработчик клавиши
                { Close(); }

                base.OnUpdateFrame(e);

            }

            /// <summary>
            /// Метод при обновления кадров в окне<br/> 
            /// <i>(Для отрисовки)</i><br/>
            /// Связан с <see cref="OnUpdateFrame">функией</see> <i><see cref="OnUpdateFrame"/></i>
            /// </summary>
            /// <param name="e"></param>
            protected override void OnRenderFrame(FrameEventArgs e)
            {
                // Изменение цвета фона
                GL.ClearColor(red: colorFrameRed,
                              green: colorFrameGreen,
                              blue: colorFrameBlue,
                              alpha: 1f);
                GL.Clear(ClearBufferMask.ColorBufferBit);
                SwapBuffers();
                base.OnRenderFrame(e);

            }
            /// <summary>
            /// Метод обработки действий при закрытии окна
            /// </summary>
            protected override void OnUnload()
            {
                base.OnUnload();
            }

            #endregion

            #region Вспомогательные функции

            /// <summary>
            /// Вычисляет FPS <br/>
            /// <i>и выводит в заглоовок окна</i>
            /// </summary>
            /// <param name="e"></param>
            private void UpdateFPS(FrameEventArgs e)
            {
                fpsTime += (float)e.Time;
                fps++;
                if (fpsTime > 1.0f)
                {
                    Title = $"MyBaby have FPS: {fps}";
                    fps = 0;
                    fpsTime = .0f;
                }
            }

            /// <summary>
            /// Изменения цвета окна в реальном времени
            /// </summary>
            private void Diskoteka()
            {
                if (colorFrameRed > 1f || colorFrameRed < .0f)
                    colorFrameRedUpdate *= -1;
                colorFrameRed += colorFrameRedUpdate;

                if (colorFrameGreen > 1f || colorFrameGreen < .0f)
                    colorFrameGreenUpdate *= -1;
                colorFrameGreen += colorFrameGreenUpdate;

                if (colorFrameBlue > 1f || colorFrameBlue < .0f)
                    colorFrameBlueUpdate *= -1;
                colorFrameBlue += colorFrameBlueUpdate;
            }

            #endregion
        }

        #endregion

        #region Управляющая функция приложения

        /// <summary>
        /// Управляющая функция приложения
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Задание размеров окна
            int width = 800, height = 500;
            // Задание первоначальных настроек при создании окна
            var windowSettings = new NativeWindowSettings()
            {
                Title = "MyBaby",
                Size = new Vector2i(width, height), //--> Задание размеров окна
                // Задание положения окна
                Location = new Vector2i(System.Windows.Forms.Screen.AllScreens.Max(s => s.Bounds.Width) / 2 - width / 2,
                                        System.Windows.Forms.Screen.AllScreens.Max(s => s.Bounds.Height) / 2 - height / 2),
                WindowBorder = WindowBorder.Fixed, //--> Шапка окна
                WindowState = WindowState.Normal, //--> Параметры открытия окна (FullScreen,Minimized)
                StartVisible = true, //--> Видимость окна при открытии
                StartFocused = true, //--> Фокус приложения при открытии

                APIVersion = new Version(4, 0), //--> Версия OpenTK
                API = ContextAPI.OpenGL, //--> Использовние OpenGL в окне
                Flags = ContextFlags.Default,
                Profile = ContextProfile.Core
            };


            using (FutureGame futureGame = new FutureGame(GameWindowSettings.Default, windowSettings))
            {
                futureGame.Run(); //--> Запуск окна
            }
        }

        #endregion
    }
}
